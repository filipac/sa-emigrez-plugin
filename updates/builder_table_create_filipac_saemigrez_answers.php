<?php namespace Filipac\SaEmigrez\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateFilipacSaemigrezAnswers extends Migration
{
    public function up()
    {
        Schema::create('filipac_saemigrez_answers', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->smallInteger('answer')->default(1);
            $table->string('name');
            $table->text('details');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('filipac_saemigrez_answers');
    }
}
